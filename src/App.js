import React from 'react'
import Paragraph from './content/paragraph';
import Middle from './content/middle';
import Menu from './content/menu';
import Header from './content/header';

function App() {
  return (
    <div className='App'>
      <Header />
      <Menu />
      <Middle />
      <Paragraph />

    </div>


  );
}




export default App;

