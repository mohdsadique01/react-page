import React from 'react'
import "./middle.css"
const content1 = () => {
  return (
    <div className='box'>
      <div className='head'> <h1 className='heading-color'>React</h1></div>
      <div><h2 className='title'>A JavaScript library for building user interfaces</h2></div>
      <div className='butondiv'>
        <div> <button className='button'> Get started </button></div>
        <div> <h3 className='h3'>Take the Tutorial</h3></div>
      </div>
    </div>
    
  )
}

export default content1
