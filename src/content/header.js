import './header.css';

const Header=()=>{
    return(
        <div className="hedr">
        <span className="supt-ukr">Support Ukraine 🇺🇦 </span>
        <span className="spn-ukr">Help Provide Humanitarian Aid to Ukraine.</span>
    </div>
    );
}

export default Header